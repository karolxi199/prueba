package com.example.ximena.merofortiskarol;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText Cedula, Nombres,Apellidos, Area,Numdepartamento,NumOficina,FechaNacimiento;
    Button Guardar,Consulatar;
    private Object lectura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Guardar = (Button)findViewById(R.id.btnguardar);
        Consulatar = (Button)findViewById(R.id.btnconsultar);

        Cedula= (EditText)findViewById(R.id.txtCedula);
        Nombres= (EditText)findViewById(R.id.txtNombres);
        Apellidos= (EditText)findViewById(R.id.txtApellidos);
        Area = (EditText) findViewById(R.id.txtarea);
        NumOficina = (EditText) findViewById(R.id.txtnumoficina);
        FechaNacimiento = (EditText) findViewById(R.id.txtnacimiento);
        final HelperBD helperbd = new HelperBD(getApplicationContext());




        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = helperbd.getWritableDatabase();
                ContentValues valores = new ContentValues();
                valores.put("cedula",Cedula.getText().toString());
                valores.put("nombres", Nombres.getText().toString());
                valores.put("apellidos", Apellidos.getText().toString());
                valores.put("area ", Area.getText().toString());
                valores.put("oficina", NumOficina.getText().toString());
                valores.put("nacimiento", FechaNacimiento.getText().toString());


                Long IdGuardado = db.insert("Secretaria", "cedula",  valores);
                Toast.makeText(getApplicationContext(),
                        "Se guardo el dato: "+IdGuardado+ Nombres.getText().toString(),
                        Toast.LENGTH_LONG).show();

            }
        });
      /* Consulatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String consulta = "";
                SQLiteDatabase db = helperbd.getReadableDatabase();
                String[] lectura = Cedula.getText().toString()};
                String[] projection = {"cedula", "nombres","apellidos","area","oficina","nacimiento"};
                Cursor c = db.query("calificaciones", projection, "codigo" + "=?", lectura, null, null, null);

                Cedula.moveToFirst();
                Nombres.setText(c.getString(0));
                Apellidos.setText(c.getString(1));
                Area.setText(c.getString(2));
                NumOficina.setText(c.getString(3));
                FechaNacimiento.setText(c.getString(4));

            }
        });*/







    }
}
