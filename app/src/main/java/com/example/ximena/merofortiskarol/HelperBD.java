package com.example.ximena.merofortiskarol;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by ALexander on 01/07/2018.
 */

public class HelperBD extends SQLiteOpenHelper {

    private static final String TABLE_SECRETARIA =
            "CREATE TABLE  secretaria (cedula text,  nombres text, apellidos text, area text, oficina text,nacimiento text)";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MiBasedeDatos.db";


    public HelperBD(Context context) {
        super(context, DATABASE_NAME, null,  DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_SECRETARIA);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS secretaria");
        onCreate(db);

    }


    public String LeerTodo() {

        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM secretaria", null);
        if (cursor.moveToFirst()){
            do{
                String Secreced = cursor.getString(cursor.getColumnIndex("cedula"));
                String SecreNombre = cursor.getString(cursor.getColumnIndex("nombres"));
                String Secreapellidos = cursor.getString(cursor.getColumnIndex("apellidos"));
                String secrearea= cursor.getString(cursor.getColumnIndex("area"));
                String secreoficina   =   cursor.getString(cursor.getColumnIndex("oficina"));
                String secrenacimiento   =   cursor.getString(cursor.getColumnIndex("nacimiento"));




                consulta += "cedula:"+Secreced + "|nombres:" + SecreNombre + "| apellidos::" + Secreapellidos + "|Area:" + secrearea + "|oficina" + secreoficina +"|nacimiento:" + secrenacimiento + "\n";
            }while (cursor.moveToNext());
        }
        return consulta;
    }
}
